//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sat Aug 22 15:34:57 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=8,numReposBlks=8,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (sys_clock);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.SYS_CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.SYS_CLOCK, CLK_DOMAIN design_1_sys_clock, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input sys_clock;

  wire HMC769_0_start_adc_count;
  wire clk_wiz_0_clk_out1;
  wire [31:0]counter_0_count;
  wire counter_0_ready;
  wire [31:0]counter_1_count;
  wire s00_axi_aclk_0_1;
  wire [15:0]xlconstant_0_dout;
  wire [15:0]xlconstant_1_dout;
  wire [0:0]xlconstant_2_dout;

  assign s00_axi_aclk_0_1 = sys_clock;
  design_1_AVERAG_0_0 AVERAG_0
       (.data_adc_1(counter_0_count[15:0]),
        .data_adc_2(counter_1_count[15:0]),
        .frame_size(xlconstant_1_dout),
        .m00_axis_aclk(counter_0_ready),
        .m00_axis_aresetn(HMC769_0_start_adc_count),
        .m00_axis_tready(1'b1));
  design_1_HMC769_0_0 HMC769_0
       (.pll_ld_sdo(1'b0),
        .s00_axi_aclk(s00_axi_aclk_0_1),
        .s00_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_aresetn(xlconstant_2_dout),
        .s00_axi_arprot({1'b0,1'b0,1'b0}),
        .s00_axi_arvalid(1'b0),
        .s00_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_awprot({1'b0,1'b0,1'b0}),
        .s00_axi_awvalid(1'b0),
        .s00_axi_bready(1'b0),
        .s00_axi_rready(1'b0),
        .s00_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s00_axi_wvalid(1'b0),
        .shift_front({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .start_adc_count(HMC769_0_start_adc_count));
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(s00_axi_aclk_0_1),
        .clk_out1(clk_wiz_0_clk_out1));
  design_1_counter_0_0 counter_0
       (.FrameSize(xlconstant_0_dout),
        .clk(clk_wiz_0_clk_out1),
        .count(counter_0_count),
        .ready(counter_0_ready),
        .reset(HMC769_0_start_adc_count));
  design_1_counter_1_0 counter_1
       (.FrameSize(xlconstant_0_dout),
        .clk(clk_wiz_0_clk_out1),
        .count(counter_1_count),
        .reset(HMC769_0_start_adc_count));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_1_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_1_xlconstant_2_0 xlconstant_2
       (.dout(xlconstant_2_dout));
endmodule
