
`timescale 1 ns / 1 ps

	module AVERAG_ADC_DATA_v1_0_M00_AXIS #
	(
		parameter integer C_M_AXIS_TDATA_WIDTH	= 64,
		parameter integer C_M_START_COUNT	    = 64
	)   
	(
		input wire  M_AXIS_ACLK,
		input wire  M_AXIS_ARESETN,
		output wire  M_AXIS_TVALID,
		input wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA_in,
		output wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA,
		output wire  M_AXIS_TLAST,
		input wire  M_AXIS_TREADY,
		
		output wire  [15:0] word_counter,
		input wire  [15:0] frame_size
	);
	
	reg [15:0] word_counter_r;
	assign word_counter = word_counter_r;
	
	assign M_AXIS_TDATA = M_AXIS_TDATA_in;
	assign M_AXIS_TLAST = (word_counter_r == (frame_size-1)) ? 1:0;
	assign M_AXIS_TVALID = ((word_counter_r >= 0)&(word_counter_r <= (frame_size-1))) ? 1:0;
	
	always @ (posedge M_AXIS_ACLK) begin
	   if(M_AXIS_TREADY & M_AXIS_ARESETN) begin
	           if(word_counter_r != (frame_size-1))  
	               word_counter_r <= word_counter_r + 1;
	           else                	               
	               word_counter_r <= 0;
	   end
	end


	endmodule
