
`timescale 1 ns / 1 ps

	module AVERAG_ADC_DATA_v1_0 #
	(
		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 64,
		parameter integer C_M00_AXIS_START_COUNT	= 64
	)  
	(
	    input wire  [15:0] data_adc_1,
	    input wire  [15:0] data_adc_2,
		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		output wire  [15:0] word_counter,
		output wire  [15:0] frame,
		input wire   [15:0] frame_size
	);
// Instantiation of Axi Bus Interface M00_AXIS
	AVERAG_ADC_DATA_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) AVERAG_ADC_DATA_v1_0_M00_AXIS_inst (
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalid),
		.M_AXIS_TDATA_in({16'H0000, data_adc_2, 16'H0000, data_adc_1}),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TLAST(m00_axis_tlast),
		.M_AXIS_TREADY(1'b1),
		
		.frame_size(frame_size),
		.frame(frame),
		.word_counter(word_counter)
	);

	// Add user logic here

	// User logic ends

	endmodule
