`timescale 1 ns / 1 ps

	module AVERAG_ADC_DATA_v1_0 #
	(
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 64,
		parameter integer C_M00_AXIS_START_COUNT	= 64
	)  
	(
	    input wire  [15:0] data_adc_1,
	    input wire  [15:0] data_adc_2,		
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,

		output wire  [15:0] word_counter,
		output reg   [15:0] frame,
		input wire   [15:0] frame_size
	);
	
	wire we;
    reg stop;
    wire EnData;
    
	AVERAG_ADC_DATA_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) AVERAG_ADC_DATA_v1_0_M00_AXIS_inst (
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalid),
		.EnData(EnData),
		.M_AXIS_TDATA_in({16'H0000, data_adc_2, 16'H0000, data_adc_1}),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TLAST(m00_axis_tlast),
		.M_AXIS_TREADY(1'b1),
		.frame_size(frame_size)
	);
	    
	reg [31:0] mem_array_1 [0:8191];
    reg [31:0] mem_array_2 [0:8191];
    reg [15:0] data_out_mem_1;
	reg [15:0] data_out_mem_2;
    reg [15:0] adr;
    reg [15:0] word_counter_r;
	assign word_counter = word_counter_r;
	
    assign EnData = (we==0 & stop==0) ? 1 : 0;
    assign m00_axis_tlast = (word_counter_r == (frame_size-2) & ~we & ~stop) ? 1:0;
	assign m00_axis_tvalid = ((word_counter_r > 0)&(word_counter_r <= (frame_size-2)) & ~we & ~stop) ? 1:0;
    assign we = (frame != 8) ? 1:0;
    
    always @ (posedge m00_axis_aclk) begin
        if(m00_axis_aresetn & ~stop) begin
            if(we == 1)
                if(frame == 0) begin
                    mem_array_1[adr] <= data_adc_1;
                    mem_array_2[adr] <= data_adc_2;
                end
                else begin
                    mem_array_1[adr] <= mem_array_1[adr] + data_adc_1;
                    mem_array_2[adr] <= mem_array_2[adr] + data_adc_2;                    
                end
            else begin
                data_out_mem_1 <= (mem_array_1[adr] >> 3);
                data_out_mem_2 <= (mem_array_2[adr] >> 3);    
            end        
        end
    end
    always @ (negedge m00_axis_aclk) begin
        if(m00_axis_aresetn) begin
            if(adr != (frame_size-1)) begin
                adr <= adr + 1;
            end
            else begin
                stop <= 1;
                adr <= 0;
            end            
        end
        else begin
            adr <= 0;
            stop <= 0;  
        end
    end
    always @ (posedge m00_axis_aclk) begin
	   if(m00_axis_aresetn & ~stop) begin
	           if(word_counter_r != (frame_size-1))  
	               word_counter_r <= word_counter_r + 1;
	           else                	               
	               word_counter_r <= 0;
	   end
	   else begin
	       word_counter_r <= 0;
	   end
	end 
    always @ (posedge m00_axis_aresetn) begin
        if(frame != 8)  
	       frame <= frame + 1;
	    else                	               
	       frame <= 0;
	end

endmodule
