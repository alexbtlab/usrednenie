`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.08.2020 12:11:51
// Design Name: 
// Module Name: tb_aver
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_aver(

    );
    
  reg clk;  
    
    initial begin
        clk = 0;
        forever #5 clk = ~clk;
    end
    
    design_1_wrapper dut
   ( 
        .sys_clock(clk) 
   );
    
endmodule
